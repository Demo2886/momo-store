# Momo Store aka Пельменная №2

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

## Оглавление <!-- omit in toc -->
- [Описание проекта](#описание-проекта)
  - [Задание](#задание)
  - [Репозитории](#репозитории)
  - [Технологии](#технологии)
  - [Публичные Helm charts](#публичные-helm-charts)
  - [Развернутые ресурсы](#развернутые-ресурсы)
  - [Frontend](#frontend)
  - [Backend](#backend)
  - [Описание работы pipelin](#описание-работы-pipelin)
- [Стратегии деплоя](#стратегии-деплоя)
- [Infrastructure](#infrastructure)
- [Screenshot](#screenshot)
  - [Grafana](#grafana)
  - [ArgoCD](#argocd)

## Описание проекта
Данный проект разработан  в учебных целях ЯндексПрактикум по направлению "DevOps для эксплуатации и разработки".
По исходному заданию https://gitlab.com/tekliuk-pet-project/momo-store-initial-task

### Задание 
 * Код хранится в GitLab с использованием любого git-flow
 * В проекте присутствует .gitlab-ci.yml, в котором описаны шаги сборки
 * Артефакты сборки (бинарные файлы, docker-образы или др.) публикуются в систему хранения (Nexus или аналоги)
 * Артефакты сборки версионируются
 * Написаны Dockerfile'ы для сборки Docker-образов бэкенда и фронтенда
 * Бэкенд: бинарный файл Go в Docker-образе
 * Фронтенд: HTML-страница раздаётся с Nginx
 * В GitLab CI описан шаг сборки и публикации артефактов
 * В GitLab CI описан шаг тестирования
 * В GitLab CI описан шаг деплоя
 * Развёрнут Kubernetes-кластер в облаке
 * Kubernetes-кластер описан в виде кода, и код хранится в репозитории GitLab
 * Конфигурация всех необходимых ресурсов описана согласно IaC
 * Состояние Terraform'а хранится в S3
 * Картинки, которые использует сайт, или другие не бинарные файлы, необходимые для работы, хранятся в S3
 * Секреты не хранятся в открытом виде
 * Написаны Kubernetes-манифесты для публикации приложения
 * Написан Helm-чарт для публикации приложения
 * Helm-чарты публикуются и версионируются в Nexus
 * Приложение подключено к системам логирования и мониторинга
 * Есть дашборд, в котором можно посмотреть логи и состояние приложения

### Репозитории:
  * [momo-store](https://gitlab.com/tekliuk-pet-project/momo-store)
  * [momo-infrastructure](https://gitlab.com/tekliuk-pet-project/momo-infrastructure)

### Технологии:
  * YC
  * Git
  * GitLab CI/CD
  * Docker
  * Nginx
  * SonarQube
  * Nexus
  * Minio
  * Terraform
  * Ansible
  * Helm
  * Kubernetes
  * ArgoCD
  * Grafana
  * Prometheus
  * Loki

### Публичные Helm charts:


| Имя | url |
|-----|------|
| Grafana | https://grafana.github.io/helm-charts |
| Loki    | https://grafana.github.io/helm-charts |
| Minio    | oci://registry-1.docker.io/bitnamicharts |
| Nexus    | https://stevehipwell.github.io/helm-charts/ |
| SonarQube    | https://stevehipwell.github.io/helm-charts/ |
| Prometheus    | https://prometheus-community.github.io/helm-charts/ |
| ArgoCD    | oci://registry-1.docker.io/bitnamicharts/ |

### Развернутые ресурсы
 * https://momo-store.tekliuk.ru
 * https://t.me/v_tekliuk_pet
 * https://my-minio.tekliuk.ru
 * https://nexus.tekliuk.ru
 * https://sonarqube.tekliuk.ru
 * https://argocd.tekliuk.ru
 * https://grafana.tekliuk.ru
 * https://prometheus.tekliuk.ru


### Frontend
Фронтенд-контейнер строится на основе образа `node:16.13.2` и публикуется в `nginx:1.25.1`.
При построении необходимо указать следующие переменные:
```dockerfile
ENV VUE_APP_API_URL=/
ENV NODE_ENV=/
```
После сборки контейнер помещается в `registry.gitlab.com/tekliuk-pet-project/momo-store/no-checked-frontend` а после проверяется  с помощью Postman если проверка пройдена то перетегирует и помещает в `registry.gitlab.com/tekliuk-pet-project/momo-store/momo-frontend` c  версией. Коллекции для Postman находятся в `/templates/collection`.

Докерфайл:
```dockerfile
FROM node:16-alpine3.16 as builder
WORKDIR /app
COPY . . 
ENV VUE_APP_API_URL=/
ENV NODE_ENV=/
RUN  npm ci --include=dev \
&& npm run build 

FROM nginx:1.25.1
WORKDIR /usr/share/app
COPY --from=builder /app/dist/ /usr/share/app
COPY --from=builder /app/frontend.conf /etc/nginx/conf.d/
```

В `frontend.conf` настраиваем переадресацию на backend.
```conf
server {

          listen 8081;
          #proxy_cache my_cache;
          #proxy_cache_valid 200 60m;
          #proxy_cache_valid 404 1m;
          #proxy_cache_key "$scheme$request_method$host$request_uri";

          location / {
                root  /usr/share/app;
                index index.html;
                try_files $uri $uri/ /index.html;
          }

          location ~ ^/(?:products|categories|orders|metrics|auth/whoami/catalog) {
            proxy_pass   http://backend:8081;
          }
}

```
### Backend
Сборка контейнера на базе образа `golang:latest` и затем c сборки копируется в контейнер на базе образа `alpine:latest` для минимализации итогового размера контейнера.
После сборки контейнер помещается в `registry.gitlab.com/tekliuk-pet-project/momo-store/no-checked-backend` а после проверяется  с помощью Postman если проверка пройдена то перетегирует и помещает в `registry.gitlab.com/tekliuk-pet-project/momo-store/momo-backend` c  версией. Коллекции для Postman находятся в `/templates/collection`.

Dockerfile:
```dockerfile
FROM golang:latest as builder
WORKDIR /app
#copy go.mod go.sum and run go mod download for cashe
COPY go.mod go.sum ./
RUN go mod download
COPY . . 
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main ./cmd/api

FROM alpine:latest  
WORKDIR /app/.
RUN addgroup --system gouser \
    && adduser -S -s /bin/false -G gouser gouser -D -H \
    && apk add --no-cache dumb-init==1.2.5-r2
COPY --chown=gouser:gouser --from=builder /app/main .
USER gouser
ENTRYPOINT ["dumb-init", "./main"] 

```
### Описание работы pipelin
Так как  job повторяются в frontend и backend выносим их в отдельную папку `/jobs/` и подключаю к проекту  с помощью `include`.

Cтадии pipeline:

  * `start-notify` (Сообщение в телеграм [группу](https://t.me/v_tekliuk_pet) о старта pipeline).
  * `test` (выполняются sast тесты gitlab sast + sonarqube).
  * `build` (Построение изображения с помощью dockerfile, тут предоставляем возможность использовать  либо `build.yml`, либо `build-kaniko.yml` для того что бы собрать инструментом `kaniko` нужно в коменте указать `kaniko`).
  * `test-images` (Проверка  состояния и работы контейнера).
  * `release` (Публикация проверенных контейнеров в Container Registry).
  * `delivery` (Запуск сборки новой версии хелм чарта в https://gitlab.com/tekliuk-pet-project/momo-infrastructure, деплой осуществлен с помощью ArgoCD)
  * `finish-notify`(Уведомление в телеграм [группу](https://t.me/v_tekliuk_pet) об удачном окончании pipeline
  * `notify` (Уведомление в телеграм [группу](https://t.me/v_tekliuk_pet) о не удачном окончании pipeline

Схемы работы pipelin:
```mermaid
graph TD;
  SubGraph1 --> SubGraph1Flow
  subgraph "momo-infrastructure"
  SubGraph1Flow(release-momo-chart)
  end

  subgraph "Momo Store Backend"
  notify_start --> semgrep-sast
  notify_start --> sonarqube-check
  semgrep-sast --> build
  sonarqube-check --> build
  build --> container_scanning
  build --> test-images
  container_scanning --> release-images
  test-images --> release-images
  release-images --> SubGraph1[delivery]
  SubGraph1[delivery] --> notify_finish
  notify_finish --> notify_error
end
```
```mermaid
graph TD;
  SubGraph1 --> SubGraph1Flow
  subgraph "momo-infrastructure"
  SubGraph1Flow(release-momo-chart)
  end

  subgraph "Momo Store Frontend"
  notify_start --> semgrep-sast
  notify_start --> sonarqube-check
  notify_start --> nodejs-scan-sast
  nodejs-scan-sast --> build
  semgrep-sast --> build
  sonarqube-check --> build
  build --> container_scanning
  build --> test-images
  container_scanning --> release-images
  test-images --> release-images
  release-images --> SubGraph1[delivery]
  SubGraph1[delivery] --> notify_finish
  notify_finish --> notify_error
end
```

## Стратегии деплоя
При изменении в любой ветке `frontend/**/*`,  `backend/**/*` запускается pipeline. По окончанию pipeline в nexus складывается helm chart, который в свою очередь обновляет ArgoCD.

## Infrastructure
Подробное описание данной главы будет находиться в [momo-infrastructure](https://gitlab.com/tekliuk-pet-project/momo-infrastructure).
## Screenshot
### Grafana

user: `viewer`
pas:  `viewer`

<img width="900" alt="image" src="https://storage.yandexcloud.net/screens/grafana1.jpg">
<img width="900" alt="image" src="https://storage.yandexcloud.net/screens/grafana2.jpg">

### ArgoCD 
Деплой приложения реализован через приложение ArgoCD 

<img width="900" alt="jpeg"  src="https://storage.yandexcloud.net/screens/ArgoCD.jpg">

