#!/bin/bash
TIME="10"
URL="https://api.telegram.org/$TELEGRAM_BOT_TOKEN/sendMessage"
TEXT="Status: $1$1$1
Project:+$CI_PROJECT_NAME-$TYPE_APP-$VERSION"

curl -s --max-time $TIME -d "chat_id=$TG_NUM_GR&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null