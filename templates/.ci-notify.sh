URL="https://api.telegram.org/$TELEGRAM_BOT_TOKEN/sendMessage"

notify(){
    if [ "$TYPE_APP" = "frontend" ]; then
        curl -X POST -H 'Content-type:application/json' --data '{"chat_id":"'"$TG_NUM_GR"'", "parse_mode":"Markdown", "text":"
        Вышла новая версия '"$TYPE_APP"' сосисочной — '"$VERSION"'

        Sonarqube = [Открыть - 🕸]('"{URL_SONARCUBE_FRONTEND}"')

        URL deploy = [Frontend]('"{URL_DEPLOY_FRONTEND}"')

        Аuthor = '"$GITLAB_USER_NAME"'  "}' $URL

    elif [ "$TYPE_APP" = "backend" ]; then
        curl -X POST -H 'Content-type:application/json' --data '{"chat_id":"'"$TG_NUM_GR"'", "parse_mode":"Markdown", "text":"
        Вышла новая версия '"$TYPE_APP"' сосисочной — '"$VERSION"'
        
        Sonarqube = [Открыть - 🕸]('"{URL_SONARCUBE_BECKEND}"')

        Аuthor = '"$GITLAB_USER_NAME"'  "}' $URL
        
    else
        echo "Что-то пошло не так!"
    fi
}
notify
